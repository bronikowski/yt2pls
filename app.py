from flask import Flask, render_template, request, session, redirect, make_response
import youtube_dl
app = Flask(__name__)
app.secret_key = 'Siret'


@app.route('/', methods=['GET', 'POST'])
def index():
    url = request.form.get('url', False)
    entries = []
    if not session.get('playlist', None):
        session['playlist'] = []
    if request.method == 'POST' and url:
        ydl = youtube_dl.YoutubeDL()
        result = ydl.extract_info(url, download=False)
        title = result['title']
        thumbnails = result['thumbnails']
        if 'formats' in result:
            for f in result['formats']:
                max_asr = 0
                try:
                    f['format'].index('audio only')
                    if max_asr < f['asr']:
                        current = f
                        max_asr = f['asr']
                except ValueError:
                    continue
                except KeyError:
                    continue
            current['title'] = title
            current['thumbnails'] = thumbnails
            entries.append(current)
            session['playlist'].append(current)

    return render_template('index.html', entries=entries, playlist=session['playlist'])

@app.route('/pls/')
def playlist():
    resp = make_response("\n".join([f['url'] for f in session['playlist']]))
    resp.content_type = 'audio/x-mpegurl'
    resp.content_disposition = 'attachment; filename="plejlist.m3u"'
    return resp

@app.route('/reset/')
def reset():
    session['playlist'] = []
    return redirect('/')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
